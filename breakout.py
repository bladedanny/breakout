#Breakout clone using pygame to learn
import pygame
import random
pygame.init()
#CONST
WIDTH = 400
HEIGHT = 600
BGCOLOUR = (0,0,0)
FGCOLOUR = (255,255,255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
COLOURS = [RED, GREEN, BLUE, YELLOW]

#Define Classes

class ball:
    def __init__(self, xpos, ypos, vel, size):
        self.xpos = xpos
        self.ypos = ypos
        self.xvel = vel
        self.yvel = vel
        self.size = size

    def drawball(self):
        pygame.draw.rect(screen, FGCOLOUR, [self.xpos, self.ypos, self.size, self.size])

    def moveball(self):
        self.xpos = self.xpos + self.xvel
        self.ypos = self.ypos + self.yvel

    def wallcollide(self):
        if self.xpos < 10:
            self.xvel = self.xvel * -1
        elif self.xpos > WIDTH - 10 - self.size:
            self.xvel = self.xvel * -1
        elif self.ypos < 10:
            self.yvel = self.yvel * -1

    def paddlecollide(self, paddle):
        data = paddle.getPos()
        px = data[0]
        pw = data[1]
        py = data[2]

        if self.xpos + self.size > px and self.xpos < px+pw and self.ypos+self.size > py:
            if not self.ypos + self.size > py + 1:
                self.yvel = self.yvel * -1

    def brickcollide(self, bricks):
        temp = []
        bounce = False
        for brick in bricks:
            data = brick.getPos()
            bx = data[0]
            by = data[1]
            bw = data[2]
            bh = data[3]

            if self.ypos < by + bh and self.ypos + self.size > by and self.xpos + self.size > bx and self.xpos < bx + bw and not bounce:
                self.yvel = self.yvel * -1
                bounce = True
            else:
                temp.append(brick)
        return temp
    
class wall:
    def __init__(self, xpos, ypos, width, height):
        self.xpos = xpos
        self.ypos = ypos
        self.width = width
        self.height = height

    def drawwall(self):
        pygame.draw.rect(screen, FGCOLOUR, [self.xpos, self.ypos, self.width, self.height])

class paddle:
    def __init__(self, xpos, ypos, width, height):
        self.xpos = xpos
        self.ypos = ypos
        self.width = width
        self.height = height
        self.movespeed = 5
        self.direction = 0

    def drawpaddle(self):
        pygame.draw.rect(screen, FGCOLOUR, [self.xpos, self.ypos, self.width, self.height])

    def paddlespeed(self, setdirection):
        if setdirection == 'left':
            self.direction = self.movespeed * -1
        elif setdirection == 'right':
            self.direction = self.movespeed
        else:
            self.direction = 0

    def movepaddle(self):
        if self.xpos > 10 and self.direction <0:
            self.xpos = self.xpos + self.direction
        if self.xpos < WIDTH - 10 - self.width and self.direction >0:
            self.xpos = self.xpos + self.direction

    def getPos(self):
        return self.xpos, self.width, self.ypos

class brick:
    def __init__(self, xpos, ypos, width, height):
        self.xpos = xpos
        self.ypos = ypos
        self.width = width
        self.height = height
        self.colour = COLOURS[random.randrange(3)]

    def drawbrick(self):
        pygame.draw.rect(screen, self.colour, [self.xpos, self.ypos, self.width, self.height])

    def getPos(self):
        return self.xpos, self.ypos, self.width, self.height
    
#set up the screen
screen = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption('Breakout')
clock = pygame.time.Clock()

#make objects
#ball
mainball = ball((WIDTH/2)-20, HEIGHT-50, -3, 15)
walls = []
#top wall
walls.append(wall(0,0,WIDTH,10))
#left wall
walls.append(wall(0,0,10,HEIGHT))
#right wall
walls.append(wall(WIDTH-10,0,10,HEIGHT))
#paddle
mainpaddle = paddle((WIDTH/2)-60, HEIGHT-30, 80, 15)
#mainpaddle = paddle(0, HEIGHT-30, WIDTH, 15)

bricks = []
brickwidth = 40
brickheight = 12
rows = 5
cols = int((WIDTH - 20) / brickwidth)
for r in range(cols):
    for c in range(rows):
        bricks.append(brick(15 + (brickwidth * r) + r, 20 + (brickheight * c) + c, brickwidth, brickheight))

bricks.append(brick(100,200,40,12))

running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                mainpaddle.paddlespeed('left')
            if event.key == pygame.K_RIGHT:
                mainpaddle.paddlespeed('right')
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                mainpaddle.paddlespeed('stop')      

    #game logic
    mainball.wallcollide()
    mainball.paddlecollide(mainpaddle)
    bricks = mainball.brickcollide(bricks)
    mainball.moveball()
    mainpaddle.movepaddle()

    #draw logic
    pygame.Surface.fill(screen, BGCOLOUR)
    for wall in walls:
        wall.drawwall()
    mainball.drawball()
    mainpaddle.drawpaddle()
    for brick in bricks:
        brick.drawbrick()
    pygame.display.update()
    clock.tick(60)
pygame.quit()
